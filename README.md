# Steam Deck Customiser

Mini image based product customiser for the Steam Deck, just for funsies.

## Roadmap
Ideas for the future:
 - Multiple angles
 - More regions to colour customise
 - Higher res photos to use for compositing
 - 'Share' functionality

## Contributing
If you're interested in contributing, it's pretty easy, the project is a very basic HTML/CSS/JS project with a few images to go with it. If you have experience with basic JS, HTML Canvas, and Bootstrap, you should be able to jump in really fast and start messing around with stuff.

## License
MIT License

## Project status
For now this project is 'finished', it was a simple one day exercise. If relevant, it may be maintained in the future, but I don't foresee it having a long future as eventually the Steam Deck will come out and hopefully some 'real life' colour customisation options.

