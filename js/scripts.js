document.addEventListener("DOMContentLoaded", function () {
  window.app = new SteamDeckCustomiser();
});

window.addEventListener("resize", function () {
  window.app.update_canvas();
});

class View {
	
	constructor(label, base_url, black_url, white_url, uv_x_url, uv_y_url) {
		this.label = label;
		this.base_url = base_url;
		this.base_img = null;
		this.base_img_data = null;
		this.black_url = black_url;
		this.black_img = null;
		this.black_img_data = null;
		this.white_url = white_url;
		this.white_img = null;
		this.white_img_data = null;
		this.uv_x_url = white_url;
		this.uv_x_img = null;
		this.uv_x_img_data = null;
		this.uv_y_url = white_url;
		this.uv_y_img = null;
		this.uv_y_img_data = null;
	}
	
}

class Region {
	
	constructor(label, default_value, view_masks, overlap) {
		this.label = label;
		this.default_value = default_value;
		this.view_masks = view_masks;
		this.control = null;
		this.overlap = overlap;
	}
	
}

class Customisation {
	
	constructor(value) {
		this.value = value;
	}
	
}

// Main Application
class SteamDeckCustomiser {
	
	constructor() {
		this.init();
		this.add_view(
			"main",
			"Main",
			"main-base.jpg",
			"main-black.jpg",
			"main-white.jpg",
			"main-uv-x.png",
			"main-uv-y.png"
		);
		this.add_view(
			"buttons",
			"Buttons",
			"buttons-base.jpg",
			"buttons-black.jpg",
			"buttons-white.jpg",
			"buttons-uv-x.png",
			"buttons-uv-y.png"
		);
		this.add_view(
			"back",
			"Back",
			"back-base.jpg",
			"back-black.jpg",
			"back-white.jpg",
			"back-uv-x.png",
			"back-uv-y.png"
		);
		this.add_view(
			"triggers",
			"Triggers",
			"triggers-base.jpg",
			"triggers-black.jpg",
			"triggers-white.jpg",
			"triggers-uv-x.png",
			"triggers-uv-y.png"
		);
		this.add_region(
			"labels", "Labels", "#eeeeee", {
				"main": {url: "main-mask-labels.jpg"},
				"buttons": {url: "buttons-mask-labels.jpg"}
			}, true
		);
		this.add_region(
			"body", "Body", "#000000", {
				"main": {url: "main-mask-body.jpg"},
				"buttons": {url: "buttons-mask-body.jpg"},
				"back": {url: "back-mask-body.jpg"},
				"triggers": {url: "triggers-mask-body.jpg"}
			}
		);
		this.add_region(
			"body-front", "Body Front", "#000000", {
				"main": {url: "main-mask-body-front.jpg"},
				"buttons": {url: "buttons-mask-body-front.jpg"},
				"back": {url: "back-mask-body-front.jpg"},
				"triggers": {url: "triggers-mask-body-front.jpg"}
			}
		);
		this.add_region(
			"body-back", "Body Back", "#000000", {
				"main": {url: "main-mask-body-back.jpg"},
				"back": {url: "back-mask-body-back.jpg"},
				"triggers": {url: "triggers-mask-body-back.jpg"}
			}
		);
		this.add_region(
			"grips", "Grips", "#000000", {
				"main": {url: "main-mask-grips.jpg"},
				"buttons": {url: "buttons-mask-grips.jpg"},
				"back": {url: "back-mask-grips.jpg"},
				"triggers": {url: "triggers-mask-grips.jpg"}
			}
		);
		this.add_region(
			"grip-right", "Grip Right", "#000000", {
				"main": {url: "main-mask-grip-right.jpg"},
				"buttons": {url: "buttons-mask-grip-right.jpg"},
				"back": {url: "back-mask-grip-right.jpg"},
				"triggers": {url: "triggers-mask-grip-right.jpg"}
			}
		);
		this.add_region(
			"grip-left", "Grip Left", "#000000", {
				"main": {url: "main-mask-grip-left.jpg"},
				"back": {url: "back-mask-grip-left.jpg"}
			}
		);
		this.add_region(
			"touchpads", "Touchpads", "#000000", {
				"main": {url: "main-mask-touchpads.jpg"},
				"buttons": {url: "buttons-mask-touchpads.jpg"}
			}
		);
		this.add_region(
			"touchpad-right", "Touchpad Right", "#000000", {
				"main": {url: "main-mask-touchpad-right.jpg"},
				"buttons": {url: "buttons-mask-touchpad-right.jpg"}
			}
		);
		this.add_region(
			"touchpad-left", "Touchpad Left", "#000000", {
				"main": {url: "main-mask-touchpad-left.jpg"}
			}
		);
		this.add_region(
			"sticks", "Sticks", "#000000", {
				"main": {url: "main-mask-sticks.jpg"},
				"buttons": {url: "buttons-mask-sticks.jpg"},
				"triggers": {url: "triggers-mask-sticks.jpg"}
			}
		);
		this.add_region(
			"stick-right", "Stick Right", "#000000", {
				"main": {url: "main-mask-stick-right.jpg"},
				"buttons": {url: "buttons-mask-stick-right.jpg"},
				"triggers": {url: "triggers-mask-stick-right.jpg"}
			}
		);
		this.add_region(
			"stick-left", "Stick Left", "#000000", {
				"main": {url: "main-mask-stick-left.jpg"}
			}
		);
		this.add_region(
			"trims", "Trims", "#000000", {
				"main": {url: "main-mask-trims.jpg"},
				"buttons": {url: "buttons-mask-trims.jpg"},
				"back": {url: "back-mask-trims.jpg"},
				"triggers": {url: "triggers-mask-trims.jpg"}
			}
		);
		this.add_region(
			"trim-right", "Trim Right", "#000000", {
				"main": {url: "main-mask-trim-right.jpg"},
				"buttons": {url: "buttons-mask-trim-right.jpg"},
				"back": {url: "back-mask-trim-right.jpg"},
				"triggers": {url: "triggers-mask-trim-right.jpg"}
			}
		);
		this.add_region(
			"trim-left", "Trim Left", "#000000", {
				"main": {url: "main-mask-trim-left.jpg"},
				"back": {url: "back-mask-trim-left.jpg"}
			}
		);
		this.add_region(
			"pivots", "Pivots", "#ffffff", {
				"main": {url: "main-mask-pivots.jpg"},
				"buttons": {url: "buttons-mask-pivots.jpg"}
			}
		);
		this.add_region(
			"pivot-right", "Pivot Right", "#ffffff", {
				"main": {url: "main-mask-pivot-right.jpg"},
				"buttons": {url: "buttons-mask-pivot-right.jpg"}
			}
		);
		this.add_region(
			"pivot-left", "Pivot Left", "#ffffff", {
				"main": {url: "main-mask-pivot-left.jpg"}
			}
		);
		this.add_region(
			"buttons", "Buttons", "#000000", {
				"main": {url: "main-mask-buttons.jpg"},
				"buttons": {url: "buttons-mask-buttons.jpg"},
				"back": {url: "back-mask-buttons.jpg"},
				"triggers": {url: "triggers-mask-buttons.jpg"}
			}
		);
		this.add_region(
			"button-a", "Button A", "#000000", {
				"main": {url: "main-mask-button-a.jpg"},
				"buttons": {url: "buttons-mask-button-a.jpg"}
			}
		);
		this.add_region(
			"button-b", "Button B", "#000000", {
				"main": {url: "main-mask-button-b.jpg"},
				"buttons": {url: "buttons-mask-button-b.jpg"}
			}
		);
		this.add_region(
			"button-x", "Button X", "#000000", {
				"main": {url: "main-mask-button-x.jpg"},
				"buttons": {url: "buttons-mask-button-x.jpg"}
			}
		);
		this.add_region(
			"button-y", "Button Y", "#000000", {
				"main": {url: "main-mask-button-y.jpg"},
				"buttons": {url: "buttons-mask-button-y.jpg"}
			}
		);
		this.add_region(
			"button-menu", "Button Menu", "#000000", {
				"main": {url: "main-mask-button-menu.jpg"},
				"buttons": {url: "buttons-mask-button-menu.jpg"}
			}
		);
		this.add_region(
			"button-steam", "Button Steam", "#000000", {
				"main": {url: "main-mask-button-steam.jpg"}
			}
		);
		this.add_region(
			"button-dpad", "Button D-Pad", "#000000", {
				"main": {url: "main-mask-button-dpad.jpg"}
			}
		);
		this.add_region(
			"button-view", "Button View", "#000000", {
				"main": {url: "main-mask-button-view.jpg"}
			}
		);
		this.add_region(
			"button-l1", "Button L1", "#000000", {
				"back": {url: "back-mask-button-l1.jpg"}
			}
		);
		this.add_region(
			"button-l2", "Button L2", "#000000", {
				"back": {url: "back-mask-button-l2.jpg"}
			}
		);
		this.add_region(
			"button-l4", "Button L4", "#000000", {
				"back": {url: "back-mask-button-l4.jpg"}
			}
		);
		this.add_region(
			"button-l5", "Button L5", "#000000", {
				"back": {url: "back-mask-button-l5.jpg"}
			}
		);
		this.add_region(
			"button-r1", "Button R1", "#000000", {
				"back": {url: "back-mask-button-r1.jpg"},
				"triggers": {url: "triggers-mask-button-r1.jpg"}
			}
		);
		this.add_region(
			"button-r2", "Button R2", "#000000", {
				"back": {url: "back-mask-button-r2.jpg"},
				"triggers": {url: "triggers-mask-button-r2.jpg"}
			}
		);
		this.add_region(
			"button-r4", "Button R4", "#000000", {
				"back": {url: "back-mask-button-r4.jpg"},
				"triggers": {url: "triggers-mask-button-r4.jpg"}
			}
		);
		this.add_region(
			"button-r5", "Button R5", "#000000", {
				"back": {url: "back-mask-button-r5.jpg"},
				"triggers": {url: "triggers-mask-button-r5.jpg"}
			}
		);
		this.add_region(
			"button-power", "Button Power", "#000000", {
				"back": {url: "back-mask-button-power.jpg"},
				"triggers": {url: "triggers-mask-button-power.jpg"}
			}
		);
		this.add_region(
			"button-volume-up", "Button Volume Up", "#000000", {
				"back": {url: "back-mask-button-volume-up.jpg"}
			}
		);
		this.add_region(
			"button-volume-down", "Button Volume Down", "#000000", {
				"back": {url: "back-mask-button-volume-down.jpg"}
			}
		);
		this.add_region(
			"logo", "Logo", "#666666", {
				"back": {url: "back-mask-logo.jpg"},
				"triggers": {url: "triggers-mask-logo.jpg"}
			}, true
		);
		this.add_region(
			"button-quick-access", "Button Quick Access", "#000000", {
				"main": {url: "main-mask-button-quick-access.jpg"}
			}
		);
		this.create_offscreen_canvas();
		this.event_handlers();
		console.log(this);
		this.load_images();
	}
	
	init() {
		
		// Constants
		this.images_dir = "images/";
		this.canvas_width = 1920;
		this.canvas_height = 1080;
		this.tile_size = 32;
		
		// App state
		this.app_state = "loading_images";
		this.images_total = 0;
		this.images_loaded = 0;
		this.current_view = "main";
		
		// Offscreen canvas
		this.offscreen_canvas = null;
		
		// Views
		this.views = {};
		
		// Regions
		this.regions = {};
		
		// Customisations 
		this.customisations = {};
		
	}
	
	add_view(view_name, label, base_url, black_url, white_url, uv_x, uv_y) {
		this.views[view_name] = new View(label, base_url, black_url, white_url, uv_x, uv_y);
	}
	
	add_region(region_name, label, default_value, view_masks, overlap = false) {
		this.regions[region_name] = new Region(label, default_value, view_masks, overlap);
	}
	
	add_customisation(region_name, value) {
		this.customisations[region_name] = new Customisation(value);
	}
	
	remove_customisation(region_name) {
		if(this.customisations.hasOwnProperty(region_name)) {
			delete this.customisations[region_name];
		}
	}
	
	create_offscreen_canvas() {
		this.offscreen_canvas = document.createElement('canvas');
		this.offscreen_canvas.width = this.canvas_width;
		this.offscreen_canvas.height = this.canvas_height;
	}
	
	event_handlers() {
		
		// Attach Split option handlers
		let splits = document.getElementsByClassName("split-toggle");
		for(let i = 0; i < splits.length; i++) {
			let elem = splits[i];
			let off = elem.dataset.off;
			let on = elem.dataset.on;
			let off_regions = elem.dataset.offregions.split(" ");
			let on_regions = elem.dataset.onregions.split(" ");
			elem.addEventListener('change', () => {
				console.log(elem.checked);
				if(elem.checked) {
					document.getElementById(off).style.display = 'none';
					document.getElementById(on).style.display = 'flex';
					for(let region of on_regions) {
						this.add_customisation(region, this.regions[region].default_value);
					}
					for(let region of off_regions) {
						this.remove_customisation(region);
					}
					this.update_canvas();
					this.update_controls();
				}
				if(elem.checked==false) {
					for(let region of off_regions) {
						this.add_customisation(region, this.regions[region].default_value);
					}
					for(let region of on_regions) {
						this.remove_customisation(region);
					}
					document.getElementById(off).style.display = 'flex';
					document.getElementById(on).style.display = 'none';
					this.update_canvas();
					this.update_controls();
				}
			});
			for(let region of off_regions) {
				this.add_customisation(region, this.regions[region].default_value);
			}
			document.getElementById(off).style.display = 'flex';
			document.getElementById(on).style.display = 'none';
			this.update_controls();
		}
		
		// Attach Colour option handlers
		let options = document.getElementsByClassName("colour-option");
		for(let i = 0; i < options.length; i++) {
			let elem = options[i];
			let region = elem.dataset.region;
			console.log(region);
			let autoadd = elem.dataset.autoadd;
			let rgb = elem.value;
			elem.addEventListener('change', () => {
				let region = elem.dataset.region;
				let rgb = elem.value;
				this.set(region, rgb);
			});
			console.log("autoadd", autoadd);
			if(autoadd=="true") {
				console.log("Autoadd", region)
				this.add_customisation(region, this.regions[region].default_value);
			}
		}
		
		// Attach View option handlers
		let views = document.getElementsByClassName("view-option");
		for(let i = 0; i < views.length; i++) {
			let elem = views[i];
			let view = elem.dataset.view;
			elem.addEventListener('click', () => {
				this.set_view(view);
				for(let y = 0; y < views.length; y++) {
					views[y].classList.remove('view-active');
				}
				elem.classList.add('view-active');
			});
		}
		
		let randomise_btn = document.getElementById('randomise-colours');
		randomise_btn.addEventListener('click', () => {
			this.randomise_colours();
		});
		
		let save_image_btn = document.getElementById('save-image');
		save_image_btn.addEventListener('click', () => {
			this.save_image();
		});
		
		this.update_controls();
		
	}
	
	load_images() {
		
		// Array of loading promises
		let load_queue = [];
		
		// Load views
		for(const view_name in this.views) {
			
			let view = this.views[view_name];
			console.log("Adding to loading queue, view: ", view_name, view);
			
			// Load Base
			let load_base = new Promise((resolve, reject) => {
				let img = new Image();
				this.images_total++;
				img.onload = () => {
					view.base_img = img;
					this.images_loaded++;
					this.update_canvas();
					view.base_img_data = this.get_image_data(view.base_img);
					console.log("Loaded: ", view.base_url, " for view ", view_name);
					resolve();
				};
				img.onerror = () => {
					console.log("Error loading: ", view);
				};
				img.src = this.images_dir + view.base_url;
			});
			
			load_queue.push(load_base);
			
			// Load white
			let load_white = new Promise((resolve, reject) => {
				let img = new Image();
				this.images_total++;
				img.onload = () => {
					view.white_img = img;
					this.images_loaded++;
					this.update_canvas();
					view.white_img_data = this.get_image_data(view.white_img);
					console.log("Loaded: ", view.white_url, " for view ", view_name);
					resolve();
				};
				img.onerror = () => {
					console.log("Error loading: ", view);
				};
				img.src = this.images_dir + view.white_url;
			});
			
			load_queue.push(load_white);
			
			// Load black
			let load_black = new Promise((resolve, reject) => {
				let img = new Image();
				this.images_total++;
				img.onload = () => {
					view.black_img = img;
					this.images_loaded++;
					this.update_canvas();
					view.black_img_data = this.get_image_data(view.black_img);
					console.log("Loaded: ", view.black_url, " for view ", view_name);
					resolve();
				};
				img.onerror = () => {
					console.log("Error loading: ", view);
				};
				img.src = this.images_dir + view.black_url;
			});
			
			load_queue.push(load_black);
			
		}
		
		// Load regions
		for(const region_name in this.regions) {
			
			let region = this.regions[region_name];
			console.log("Adding to loading queue, region: ", region_name, region);
			
			for(const view_name in region.view_masks) {
				
				let view_mask_url = region.view_masks[view_name].url;
				console.log("Loading view mask: ", view_name, view_mask_url);
				
				let load_region = new Promise((resolve, reject) => {
					this.images_total++;
					let img = new Image();
					img.onload = () => {
						this.images_loaded++;
						region.view_masks[view_name].mask_img = img;
						region.view_masks[view_name].mask_img_data = this.get_image_data(img);
						region.view_masks[view_name].mask_tiles = this.get_tiles(region.view_masks[view_name].mask_img_data);
						this.update_canvas();
						console.log("Loaded: ", view_mask_url, " for region ", region_name, " for view ", view_name);
						resolve();
					};
					img.src = this.images_dir + view_mask_url;
				});
				
				load_queue.push(load_region);
				
			}

		}
		
		// Load all		
		Promise.all(load_queue).then(() => {
			this.loaded();
		});
		
	}
	
	get_image_data(img) {
		let ctx = this.offscreen_canvas.getContext('2d');
		ctx.drawImage(img, 0, 0, this.canvas_width, this.canvas_height);
		let image_data = ctx.getImageData(0, 0, this.canvas_width, this.canvas_height);
		return image_data;
	}
	
	get_tiles(img_data) {
		let tiles_x = Math.ceil(this.canvas_width / this.tile_size);
		let tiles_y = Math.ceil(this.canvas_height / this.tile_size);
		let occupied_tiles = [];
		for(let tx = 0; tx < tiles_x; tx++) {
			for(let ty = 0; ty < tiles_y; ty++) {
				let tile_x = tx * this.tile_size;
				let tile_y = ty * this.tile_size;
				let tile_width = Math.min(this.tile_size, this.canvas_width - tile_x);
				let tile_height = Math.min(this.tile_size, this.canvas_height - tile_y);
				let occupied = false;
				for(let x = tile_x; x < tile_x + tile_width; x++) {
					for(let y = tile_y; y < tile_y + tile_height; y++) {
						let mask = this.get_pixel_r(img_data, x, y);
						if(mask > 0) {
							occupied = true;
						}
					}
				}
				if(occupied) {
					let tile = {
						x: tile_x,
						y: tile_y,
						width: tile_width,
						height: tile_height
					}
					occupied_tiles.push(tile);
				}
			}
		}
		console.log("Tiles: ", tiles_x, tiles_y, "Occupied: ", occupied_tiles.length);
		return occupied_tiles;
	}
	
	loaded() {
		this.app_state = "loaded";
		document.getElementById('options').style.display = "flex";
		console.log("Loaded");
		console.log(this);
		this.update_canvas();		
	}
	
	set_view(view) {
		console.log(this, view);
		this.current_view = view;
		this.update_canvas();
	}
	
	set(region, rgb) {
		if(this.customisations.hasOwnProperty(region)) {
			this.customisations[region].value = rgb;
		}
		this.update_canvas();
	}
	
	update_controls() {
		let controls = document.getElementsByClassName("colour-option");
		for(let i = 0; i < controls.length; i++) {
			let elem = controls[i];
			let region = elem.dataset.region;
			if(this.customisations.hasOwnProperty(region)) {
				elem.value = this.customisations[region].value;
			};
		}
	}

	randomise_colours() {
		for(const region_name in this.customisations) {
			let region = this.customisations[region_name];
			let r = this.rand(0, 255);
			let g = this.rand(0, 255);
			let b = this.rand(0, 255);
			let hex = this.rgbToHex(r, g, b);
			region.value = hex;
		}
		this.update_controls();
		this.update_canvas();
	}

	save_image() {
		let canvas = document.getElementById('composite');
		let download_link = document.createElement('a');
		let dataURL = canvas.toDataURL();
		download_link.setAttribute('download', 'Custom Steam Deck.png');
		download_link.setAttribute('href', dataURL);
		document.body.appendChild(download_link);
		download_link.click();
		setTimeout(() => { document.body.removeChild(download_link);	},1000);
	}
	
	update_canvas() {
		
		let canvas = document.getElementById('composite');
		let ctx = canvas.getContext('2d');
		canvas.width = ctx.canvas.clientWidth;
		canvas.height = ctx.canvas.clientHeight;
		ctx.font = "20px Arial";
		
		switch(this.app_state) {
			
			case "loading_images":
				this.draw_loading(canvas, ctx);
				break;
				
			case "loaded":
				this.draw_deck(canvas, ctx);
				break;
				
		}
		
	}
	
	draw_loading(canvas, ctx) {
		let progress = (this.images_loaded / this.images_total) * 100;
		let progress_txt = progress.toFixed(1) + "%";
		this.draw_text_centered(canvas, ctx, "Loading. " + progress_txt);
	}
	
	draw_deck(canvas, ctx) {
		
		// Record start time.
		let start_time = performance.now();
		
		// Create offscreen canvas
		let offscreen_ctx = this.offscreen_canvas.getContext('2d');
		
		// Get image data..
		let white_img_data = this.views[this.current_view].white_img_data;
		let black_img_data = this.views[this.current_view].black_img_data;
		let base_img = this.views[this.current_view].base_img;
		
		// The image we're building..
		offscreen_ctx.clearRect(0, 0, this.canvas_width, this.canvas_height);
		offscreen_ctx.drawImage(base_img, 0, 0, this.canvas_width, this.canvas_height);
		let composited_img_data = offscreen_ctx.getImageData(0, 0, this.canvas_width, this.canvas_height);
		
		console.log("Draw, Customisations: ", this.customisations);
		
		// Now for each customisation layer.. 
		for(const [key, value] of Object.entries(this.customisations)) {
			
			let region = this.regions[key];
			if(!region.overlap) {
				console.log("Draw", key);
				if(region.view_masks.hasOwnProperty(this.current_view)) {
					let view_mask = region.view_masks[this.current_view];
					let mask_img_data = view_mask.mask_img_data;
					let mask_tiles = view_mask.mask_tiles;
					
					// Get the blend colour..
					let hex = value.value;
					let rgb8uint = this.hexToRgb(hex);
					let rgb = { r: rgb8uint.r / 255.0, g: rgb8uint.g / 255.0, b: rgb8uint.b / 255.0 };
					
					for(let tile of mask_tiles) {
						// Blend between white and black using the existing current image..
						this.colour_blend(rgb, tile.x, tile.y, tile.width, tile.height, white_img_data, black_img_data, mask_img_data, composited_img_data);
					}
					
				}
			}
			
		}
		
		// Now for each customisation layer.. 
		for(const [key, value] of Object.entries(this.customisations)) {
			
			let region = this.regions[key];
			if(region.overlap) {
				console.log("Draw", key);
				if(region.view_masks.hasOwnProperty(this.current_view)) {
					let view_mask = region.view_masks[this.current_view];
					let mask_img_data = view_mask.mask_img_data;
					let mask_tiles = view_mask.mask_tiles;
					
					// Get the blend colour..
					let hex = value.value;
					let rgb8uint = this.hexToRgb(hex);
					let rgb = { r: rgb8uint.r / 255.0, g: rgb8uint.g / 255.0, b: rgb8uint.b / 255.0 };
					
					for(let tile of mask_tiles) {
						// Blend between white and black using the existing current image..
						this.colour_blend(rgb, tile.x, tile.y, tile.width, tile.height, white_img_data, black_img_data, mask_img_data, composited_img_data);
					}
					
				}
			}
			
		}
		
		// Blend between white and black..
		offscreen_ctx.putImageData(composited_img_data, 0, 0);
		
		// Draw offscreen canvas..
		ctx.drawImage(this.offscreen_canvas, 0, 0, canvas.width, canvas.height);
		
		// Record end time.
		let end_time = performance.now();
		
		// Debug time
		console.log("Processing time: " + (end_time-start_time) + "ms.");
		
	}

	colour_blend(rgb, sx, sy, w, h, white_imgdat, black_imgdat, mask_imgdata, composited_imgdata) {
		
		let ex = sx + w;
		let ey = sy + h;
		for(let x = sx; x < ex; x++) {
			for(let y = sy; y < ey; y++) {
				let mask = this.get_pixel_r(mask_imgdata, x, y);
				if(mask > 0) {
					mask = mask / 255;
					let white = this.get_pixel(white_imgdat, x, y);
					let black = this.get_pixel(black_imgdat, x, y);
					let composite = this.get_pixel(composited_imgdata, x, y);
					let blended = {
						r: this.blend_colour(black.r, white.r, rgb.r),
						g: this.blend_colour(black.g, white.g, rgb.g),
						b: this.blend_colour(black.b, white.b, rgb.b)
					};
					blended.r = this.lerp(composite.r, blended.r, mask);
					blended.g = this.lerp(composite.g, blended.g, mask);
					blended.b = this.lerp(composite.b, blended.b, mask);
					this.set_pixel(composited_imgdata, x, y, blended);
				}
			}
		}
	}

	blend_colour(black, white, factor) {
		let diff = white - black;
		let v = (diff * factor) + black;
		return v;
	}

	lerp(start, end, amt){
		return (1 - amt) * start+amt * end;
	}

	get_pixel(imgdata, x, y) {
		let rgb = { r: 0, g: 0, b: 0};
		rgb.r = imgdata.data[((y * (this.canvas_width * 4)) + (x * 4))];
		rgb.g = imgdata.data[((y * (this.canvas_width * 4)) + (x * 4)) + 1];
		rgb.b = imgdata.data[((y * (this.canvas_width * 4)) + (x * 4)) + 2];
		return rgb;
	}

	get_pixel_r(imgdata, x, y) {
		return imgdata.data[((y * (this.canvas_width * 4)) + (x * 4))];
	}

	set_pixel(imgdata, x, y, rgb) {
		imgdata.data[((y * (this.canvas_width * 4)) + (x * 4))] = rgb.r;
		imgdata.data[((y * (this.canvas_width * 4)) + (x * 4)) + 1] = rgb.g;
		imgdata.data[((y * (this.canvas_width * 4)) + (x * 4)) + 2] = rgb.b;
	}

	draw_text_centered(canvas, ctx, text) {
		let w = ctx.canvas.clientWidth;
		let h = ctx.canvas.clientHeight;
		let tw = ctx.measureText(text).width;
		let th = 20;
		let x = (w / 2) - (tw / 2);
		let y = (h / 2) - (th / 2);
		ctx.fillText(text, x, y);
	}

	hexToRgb(hex) {
		var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
		return result ? {
			r: parseInt(result[1], 16),
			g: parseInt(result[2], 16),
			b: parseInt(result[3], 16)
		} : null;
	}

	rgbToHex(r, g, b) {
		return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
	}

	SRGBToLinear( v ) {
		return ( v < 0.04045 ) ? v * 0.0773993808 : Math.pow( v * 0.9478672986 + 0.0521327014, 2.4 );
	}

	LinearToSRGB( v ) {
		return ( v < 0.0031308 ) ? v * 12.92 : 1.055 * ( Math.pow( v, 0.41666 ) ) - 0.055;
	}
	
	rand(min, max) {
		
		if (min==null && max==null)
			return 0;    
		
		if (max == null) {
				max = min;
				min = 0;
		}
		
		return min + Math.floor(Math.random() * (max - min + 1));
		
	}
	
}
